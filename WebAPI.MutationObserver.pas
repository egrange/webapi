﻿unit WebAPI.MutationObserver;

interface

uses WebAPI.Elements;

type

   JMutationObserver = class;
   JMutationObserverInit = class;
   JMutationRecord = class;
   JMutationRecords = array of JMutationRecord;

   JMutationCallback = procedure (mutations : JMutationRecords; observer : JMutationObserver);

   JMutationObserver = class external 'MutationObserver'
      constructor Create(callback : JMutationCallback);
      procedure Observe(target : JElement; options : JMutationObserverInit); external 'observe';
      procedure Disconnect; external 'disconnect';
      function TakeRecords : JMutationRecords; external 'takeRecords';
   end;

   JMutationObserverInit = class external 'MutationObserverInit'
      childList : Boolean;
      attributes : Boolean;
      characterData : Boolean;
      subtree : Boolean;
      attributeOldValue : Boolean;
      characterDataOldValue : Boolean;
      attributeFilter : array of String;
   end;

   JMutationRecord = class external 'MutationRecord'
      &type : String; // Read Only
      target : JElement; // Read Only
      addedNodes : JElements; // Read Only
      removedNodes : JElements; // Read Only
      previousSibling : JElement; // Read Only
      nextSibling : JElement; // Read Only
      attributeName : String; // Read Only
      attributeNamespace : String; // Read Only
      oldValue : String; // Read Only
   end;

